# -*- coding: utf-8 -*-

import random
import math

def is_prime(n):
    """Check primality of n based on Miller Rabin algorithm
    It is yes biased with a probability of (1/4)^5 = 9.7656e-04"""
    i = 0
    prime = True 
    
    #write n-1 = 2^k*m
    k = 0
    m = n - 1
    while (m%2 != 0):
        k = k+1
        m = m/2
        
    #test 5 times
    while(prime and i<5):
        prime_i = False
        
        #a is a random number between 1 and n-1
        a = random.randit(1,n)
        
        #b = a^m mod n
        b = pow(a,m,n)
        
        if (b%n == 1):
            prime_i = True
        else:
            j = 0
            while (j < k):
                if (b%n == n-1):
                    prime_i = True
                    break
                else:
                    b = pow(b, 2, n)
                j = j+1
        prime = prime_i
        i = i+1
    return prime

def bezout(a, b):
    """Calculate the bezout coefficient (u,v) in au+bv=G
    """
    if b == 0: return (1, 0)
    else:
        (u, v) = bezout(b, a % b)
        return (v, u -  (a // b)*v)
    
def gcd(a, b):
    """Calculate the Greatest Common Divisor of a and b.

    Unless b==0, the result will have the same sign as b (so that when
    b is divided by it, the result comes out positive).
    """
    while b:
        a, b = b, a%b
    return a

def euclid(a,b):
    """Calculate G, u, v, in au+bv=G"""
    G = gcd(a,b)
    (u,v) = bezout(a,b)
    return (G,u,v)
    
def get_parameters():
    """Generate the parameters for RSA with n written with 1024 bits."""

    #Initialization of p and q 
    p = random.randint(2^512, 2^1024)
    q = random.randint(2^512, 2^1024)
    
    while ( p==q and not (is_prime(p)) and not (is_prime(q)) ):
        #Generate a random number between 2^512 and 2^1024
        p = random.randint(2^512, 2^1024)
        q = random.randint(2^512, 2^1024)
    
    #Computes n and phi(n)
    n = p*q
    phi = (p-1)*(q-1)

    #Initializate b
    b = max(p,q)

    #Compute gcd(b,phi) and a, with a*b+(v)*phi = G
    (G,a,v) = euclid(b, phi)
    
    #increaminting b until gcd(b,phi) = 1 and a>(n)^(1/2) with a the inverse of
    #b mod phi
    while ( G!=1 or a < math.sqrt(n) ):
        b = b+2
        (G,a,v) = euclid(b, phi)
    
    return (n,p,q,a,b)



#Get the parameters for RSA with n written with 1024 bits.
n,p,q,a,b = get_parameters();