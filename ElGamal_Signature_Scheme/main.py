# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 15:39:57 2018

@author: Symphonie Razafindrabe
"""

import numpy as np
import random
import gauss
from sympy import Matrix

def verify_signature(x,gamma,delta,p,alpha,beta):
    """Verify the signature (gamma, delta) for the message x, 
    encoded with a public key (p, alpha, beta)"""
    
    beta_p = pow(beta, gamma) 
    gamma_p = pow(gamma, delta)
    alpha_p = pow(alpha, x, p)
    
    if (alpha_p==beta_p*gamma_p%p):
        #verification condition
        return True
    return False

def decompose(x,B):
    """Decompose x into a factor base B. 
    Return the coefficient of each factors as an array 
    Return None if x has not decomposition over the factor base"""
    prime_powers = np.zeros(len(B))
    for i in range(len(B)):
        prime = B[i]
        while (x%prime == 0):
            x = x//prime
            prime_powers[i] +=1
    if (x==1):
        return prime_powers
    else:
        return None



def find_a(p, alpha, beta, B):
    """Find private key a
    According to the Index Calculus method, we decompose C values other a factor 
        base B, alpha^(xj)=p1^c1...pB^cB
         We write this as a system of equations:
         Ax = y, with y=[x1 ... xC]', x=[log(p1) ... log(pB)]', A=[cij]
         Since A is not squared, we resort to the pseudo inverse to resolve this equation:
         (A'.A)x = A'.y, with A' the transposed of A
         x = inv(A'.A).A'.y, since A'.A is orthogonal"""
        
    b = len(B)
    C = b+10
    
    a_found = False
    
    while (not a_found):
        #Creation of the system of equations
        X = [] 
        prime_powers = []
        
        while (len(prime_powers) < C):
            xj = random.randint(0, p-1)
            if (xj not in X):
                alpha_p = pow(alpha, xj, p)
                xj_decomposition = decompose(alpha_p, B)
                if ( xj_decomposition is not None ):
                    X.append(xj)
                    prime_powers.append(xj_decomposition)
        
        #Compute a modular pseudo-inverse: 
        A = np.dot(np.transpose(prime_powers), prime_powers)
        A = Matrix(A) #convert into a Matrix to use the sympy library which enables operations into a finite field
        A_inv = A.inv_mod(p-1) #modular inverse
        A_inv = np.array(A_inv) #reconvert to a usable array
        
        #Compute Y = A'.y_old
        Y = np.dot(np.transpose(prime_powers), X)
        
        #Solve the system
        log_B = np.mod(np.dot(A_inv, Y), p-1)
        
        #Computation of log beta using a Las Vegas randomized algorithm
            #we decompose sigma = beta*alpha^s mod p, with s a random number
            #finally, we get: log beta = c1.log(p1)+...+cB.log(pB)-s mod (p-1)
            
        #Decompose sigma into a factor base
        sigma_decomposition = None
        
        while (sigma_decomposition is None):
            s = random.randint(1, p-2)
            sigma = pow(alpha, s, p)
            sigma = sigma*beta%p
            sigma_decomposition = decompose(sigma, B)
        
        #Determine log beta = a
        Coeff = np.reshape(sigma_decomposition, np.shape(log_B))
        a = np.dot(log_B, Coeff.transpose())
        a = (a-s)%(p-1)
        a = int(a)
        
        #Check if a is the correct value by verifying that beta = alpha^a mod p
        a_found = (pow(alpha, a,p) == beta)
        
        return a



def find_k(p, delta, gamma):
    """Find private key k
        We would like to resolve delta*k = (x-a*gamma) mod (p-1)
        If gcd(delta, p-1)=1, we get k by inversing delta
        Otherwise, we simplify by gcd(delta, p-1) to get delta'*k = (x-a*gamma)' mod (p'), 
        with gcd(delta', p')=1"""
        
    d = gauss.egcd(delta, p-1)
    d = d[0]
    
    delta_p = delta//d
    x_p = (x-a*gamma)%(p-1)
    x_p = x_p//d
    p_p = (p-1)//d
    
    eps = gauss.modinv(delta_p, p_p)
    k = (eps*x_p)%p_p
    
    # k = delta'^(-1)*(x-a*gamma)' mod (p') = k'
    # so, k = k' + i*(p-1), with i to determine
    # we iterate through i until we find one that satisfies the equation: gamma = alpha^k mod p
    while (pow(alpha,k,p)!=gamma):
        k += p_p
        
    return k

#parameters
p = 31847
alpha = 5
beta = 26379
    
x = 20543
gamma = 20679
delta = 11082

#Verify signature
signature_verified = verify_signature(x,gamma,delta,p,alpha,beta)

if (signature_verified):
    print("[VERIFIED] The signature (%s, %s) is verified for the message %s" % (gamma, delta, x) )
else:
    print("[UNVERIFIED] The signature (%s, %s) is not verified for the message %s" % (gamma, delta, x) )

#Find a
B = (2, 3, 5, 7, 11)
a = find_a(p,alpha,beta,B)

print("Private key a: %s" %(a))

#Find k
k = find_k(p, delta, gamma)
print("Private key k: %s" %(k))